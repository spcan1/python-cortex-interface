import socketserver

from cortex_interface import logger


class TCHandler(socketserver.BaseRequestHandler):
    """Handler for the Telemetry server."""

    def handle(self):
        logger.info('New Default Handler built. Closing connection...')
