import queue
import socket
import socketserver
import time

from datetime import date, datetime
from select import select

from cortex_interface import logger
from cortex_interface.common.constants import TMRequestType
from cortex_interface.common.messages import UNIDENTIFIED, REJECTION,\
    ACKNOWLEDGE, TelemetryRequest, TelemetryPacket

from cortex_interface.common.error import sockerror


TEST_PACKET = bytes([i % 256 for i in range(0, 128)])


class TMHandler(socketserver.BaseRequestHandler):
    """Handler for the Telemetry server."""

    def handle(self):

        # Attempt to reserve a slot and receive a Telemetry request.
        request = self.connect()

        if TMRequestType.is_dummy(request):
            self.dummy_telemetry()

        elif TMRequestType.is_single_frame(request):
            self.single_frame()

        elif TMRequestType.is_permanent(request):
            self.permanent_frames()

        elif request is None:
            logger.warn('No request received on TM connection. Closing...')

        else:
            logger.error(
                f'Unknown state in TM Connection {self.id}. Closing...')

    def connect(self) -> bool:
        """
        Attempt to acquire a connection slot from the server.
        If acquired, listen to the TM Request and serve it.
        """

        # Acquire the connection lock.
        with self.server.queuelock:
            # If there are any connections available, acquire an ID.
            if self.server.slots > 0:
                self.server.slots -= 1

                # Acquire an avilable ID.
                for i in self.server.ids:
                    if i is not None:
                        self.id = i
                        self.server.ids[i] = None
                        break

            else:
                self.request.send(REJECTION)
                return False

        self.timetagcode = self.server.timetagcode

        logger.info(f'New TM Connection: ID {self.id}')

        # Wait for 30 seconds for a TM request.
        start = time.time()
        request = None
        self.channelname = None
        self.counter = 0

        while((time.time() - start) <= 30.0) and (request is None):

            # Check for changes in the connection.
            ready, _, error = select([self.request], [], [self.request], 0)

            # If the socket errored, quit.
            if self.request in error:
                logger.error(f'Socket error in TM Connection {self.id}')
                return None

            # If there is data to be read, process it.
            if self.request in ready:
                try:
                    tmreq = TelemetryRequest.recv(self.request)

                except ValueError:
                    logger.warn(f'Bad packet in TM Connection {self.id}')
                    self.request.send(UNIDENTIFIED)

                except socket.error as e:
                    if sockerror(e, self.id, 'TM Connection'):
                        return False
                    else:
                        self.request.send(UNIDENTIFIED)

                except EOFError:
                    if (self.channelname is not None) and \
                            (self.queue is not None):
                        return request
                    else:
                        logger.warn(f'Bad packet in TM Connection {self.id}')
                        self.request.send(UNIDENTIFIED)

                else:
                    # Permanent flow or single frame.
                    if tmreq.dataflow in [0, 1, 4, 5]:
                        # Create the queue.
                        msgqueue = queue.Queue(1024)
                        queuename = format('Queue{}', self.id)

                        with self.server.queuelock:
                            self.server.queues[queuename] = msgqueue

                        self.queue = msgqueue
                        self.queuename = queuename

                        if tmreq.dataflow in [0, 4]:
                            # Continuous mode.
                            request = TMRequestType.Continuous
                            return request
                        else:
                            # Single frame mode.
                            request = TMRequestType.SingleFrame
                            return request
                    elif tmreq.dataflow in [2, 6]:
                        # Dummy telemetry.
                        request = TMRequestType.Dummy
                        return request

        # Make sure to return None if it times out.
        return request

    def unwind(self):
        """Clean up method when the server shuts down."""

        # Acquire the lock.
        with self.server.queuelock:
            # Remove the queues.
            self.server.queues.pop(self.queuename)

            # Add back the ID.
            self.server.ids[self.id] = self.id
            self.server.slots += 1

    def dummy_telemetry(self):
        """Serves dummy telemetry to this connection."""

        logger.info(f'TM Connection {self.id} serving dummy telemetry.')

        while not self.server.kill:
            # Check if there was a stop flow command.
            if self.stopflow():
                break

            # Build a dummy TM packet.
            dummy = TelemetryPacket.build(
                TEST_PACKET,
                timetag=self.timetagcode,
                counter=self.counter)

            # Send the Dummy packet.
            try:
                self.request.send(dummy)
                self.counter += 1

            except socket.error as e:
                if sockerror(e, self.id, 'TM Connection'):
                    break

            time.sleep(0.01)

        logger.info(f'Closing TM Connection {self.id}...')

        # Acquire the lock.
        with self.server.queuelock:
            # Add back the ID.
            self.server.ids[self.id] = self.id
            self.server.slots += 1

        logger.info(f'TM Connection {self.id} closed.')

    def single_frame(self):
        """Serves a single telemetry frame to this connection."""

        # TODO : Fix single frame method overloading last frame.
        # WARNING : This method does not do what it is suposed to.

        logger.info(f'TM Connection {self.id} serving single frame.')

        while (not self.server.kill) and (self.counter < 1):

            # Check if there was a stop flow command.
            if self.stopflow():
                logger.info(f'TM Connection {self.id} closed by user.')
                break

            if not self.queue.empty():
                # Send the packet.
                try:
                    self.request.send(self.queue.get())
                    self.counter += 1
                    break

                except socket.error as e:
                    if sockerror(e, self.id, 'TM Connection'):
                        break

            time.sleep(0.01)

        self.unwind()

    def permanent_frames(self):
        """Serves continuous telemetry to this connection."""

        logger.info(f'TM Connection {self.id} serving continuous frames.')

        while not self.server.kill:

            # Check if there was a stop flow command.
            if self.stopflow():
                logger.info(f'TM Connection {self.id} closed by user.')
                break

            if not self.queue.empty():
                # Send the packet.
                try:
                    self.request.send(self.queue.get())
                    self.counter += 1

                except socket.error as e:
                    if sockerror(e, self.id, 'TM Connection'):
                        break

            time.sleep(0.01)

        self.unwind()

    def stopflow(self):
        """Checks if the user requested a flow stop."""

        # Check if there is data to be read.
        ready, _, _ = select([self.request], [], [], 0)

        if self.request in ready:
            try:
                tmreq = TelemetryRequest.recv(self.request)

            except ValueError:
                logger.warn(f'Bad packet in TM Connection {self.id}')
                self.request.send(UNIDENTIFIED)
                return False

            except socket.error as e:
                return sockerror(e, self.id, 'TM Connection')

            except EOFError:
                logger.warn(f'EOF Error in TM Connection {self.id} while expecting stopflow command.')
                return False

            else:
                if TMRequestType.is_stop(tmreq.dataflow):
                    logger.info(f'TM Connection {self.id} stop requested.')
                    self.request.send(ACKNOWLEDGE)
                    return True
                else:
                    return False

        else:
            return False
