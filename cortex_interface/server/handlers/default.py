import socketserver

from cortex_interface import logger


class DefaultHandler(socketserver.BaseRequestHandler):
    """
    Default Handler for the Telemetry server.
    Immediately closes any open connection.
    """

    def handle(self):
        logger.info('New Default Handler built. Closing connection...')
