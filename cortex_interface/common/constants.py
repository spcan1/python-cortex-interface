

class TMChannel:
    """Integer value of each Telemetry Channel"""
    ChannelA = 0
    ChannelB = 1
    ChannelC = 2
    ChannelD = 3
    ChannelE = 4
    ChannelF = 5


class TimetagCode:
    Code0 = 0
    Code1 = 1
    Code2 = 2
    Code3 = 3
    Code4 = 4


class TMRequestType:
    """Integer value of each Telemetry flow request."""
    Continuous = 0
    SingleFrame = 1
    Dummy = 2

    ContinuousOffline = 4
    SingleFrameOffline = 5
    DummyOffline = 6

    Stop = 0x80

    @staticmethod
    def is_dummy(val: int):
        return val in [2, 6]

    @staticmethod
    def is_single_frame(val: int):
        return val in [1, 5]

    @staticmethod
    def is_permanent(val: int):
        return val in [0, 4]

    @staticmethod
    def is_stop(val: int):
        return val == 0x80
