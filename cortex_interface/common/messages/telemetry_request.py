import xdrlib

from . import CortexMessage

from cortex_interface.common.constants import TMChannel


class TelemetryRequest(CortexMessage):
    """
    Representation of the Telemetry Request packets.
    """

    @classmethod
    def permanent(
            cls,
            channel: TMChannel,
            buffered=0,
            online=True,
            flow_id=0
            ) -> bytes:

        """Builds a Telemetry Request for a permanent flow of data."""

        if online:
            return cls.build(channel, buffered, 0, flow_id)
        else:
            return cls.build(channel, buffered, 4, flow_id)

    @classmethod
    def singleframe(
            cls,
            channel: TMChannel,
            buffered=0,
            online=True,
            flow_id=0
            ) -> bytes:

        """Builds a Telemetry Request for a single frame of data."""

        if online:
            return cls.build(channel, buffered, 1, flow_id)
        else:
            return cls.build(channel, buffered, 5, flow_id)

    @classmethod
    def dummy(
            cls,
            channel: TMChannel,
            buffered=0,
            online=True,
            flow_id=0
            ) -> bytes:

        """Builds a Telemetry Request for a permanent flow of dummy data."""

        if online:
            return cls.build(channel, buffered, 2, flow_id)
        else:
            return cls.build(channel, buffered, 6, flow_id)

    @classmethod
    def stop(cls, channel: TMChannel, flow_id=0) -> bytes:
        return cls.build(channel, 0, 0x80, flow_id)

    @classmethod
    def build(
            cls,
            channel: TMChannel,
            buffered: int,
            dataflow: int,
            flow_id: int
            ) -> bytes:

        """General build method for all Telemetry Requests"""

        # Build the packer.
        xdr = xdrlib.Packer()

        # Clamp the number of buffered frames.
        if buffered > 1024:
            buffered = 1024
        if buffered < 0:
            buffered = 0

        # Channel, buffered frames and data flow.
        xdr.pack_uint(channel)
        xdr.pack_uint(buffered)
        xdr.pack_uint(dataflow)

        # Pack other data.
        for _ in range(0, 9):
            xdr.pack_uint(0)

        return super().pack(xdr.get_buffer(), flow_id=flow_id)

    def _process(self, msg: bytes):
        """
        Overloaded method fror message processing.
        """

        # Build the Unpacker.
        xdr = xdrlib.Unpacker(msg)

        # Unpack configuration.
        self.channel = xdr.unpack_uint()
        self.buffered = xdr.unpack_uint()
        self.dataflow = xdr.unpack_uint()

        for _ in range(0, 9):
            xdr.unpack_uint()
