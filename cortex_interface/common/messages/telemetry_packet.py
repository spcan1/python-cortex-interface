import logging
import time
import xdrlib

from datetime import date, datetime

from . import CortexMessage
from cortex_interface.common.constants import TimetagCode


logger = logging.getLogger(__name__)


# Time tag base.
# WARNING: This method fails if the server does not have a reset
# after 31st December.
base_seconds = datetime(date.today().year, 1, 1).timestamp()


class TelemetryPacket(CortexMessage):
    """
    Cortex packets encapsulating Telemetry frames.
    """

    @classmethod
    def build(cls, data: bytes, flow_id=0, **kwargs) -> bytes:
        """
        Packs the given data buffer into a Cortex packet.
        Cortex Header
        Telemetry metadata (6) (zeroed)
        Telemetry buffer size
        Telemetry metadata (4) (zeroed)
        Telemetry buffer (4-byte aligned)
        Cortex postamble
        """

        # Build the XDR Packer.
        xdr = xdrlib.Packer()

        # Time tag.
        if 'timetag' in kwargs:
            timetag = kwargs['timetag']
            # Check which code to use.
            if timetag == TimetagCode.Code0:
                # Get the value delta.
                timetagval = time.time() - base_seconds

                xdr.pack_uint(int(timetagval))
                xdr.pack_uint(int(timetagval * 1000))

            elif timetag == TimetagCode.Code1:
                # Load today's datetime.
                today = datetime.today()
                date_ = today.date
                time_ = today.time

                # Load day and hour.
                timetag0 = ((date_.day & 0xFFFF) << 8) | (time_.hour & 0xFF)
                xdr.pack_uint(timetag0)

                # Load minutes, seconds and milliseconds.
                timetag1 = ((time_.minute & 0xFF) << 24) |\
                    ((time_.second & 0xFF) << 16) |\
                    ((time_.microsecond / 1000) & 0xFFFF)
                xdr.pack_uint(timetag1)

            elif timetag == TimetagCode.Code2:
                # Get the value delta.
                timetagval = time.time() - base_seconds

                # Load seconds since reference.
                xdr.pack_uint(int(timetagval))

                # Load milliseconds.
                xdr.pack_float((timetagval % 1) * 1000)

            elif timetag == TimetagCode.Code3:
                xdr.pack_uint(0)
                xdr.pack_uint(0)

            elif timetag == TimetagCode.Code4:
                xdr.pack_uint(0)
                xdr.pack_uint(0)

            elif timetag is None:
                xdr.pack_uint(0)
                xdr.pack_uint(0)

        else:
            xdr.pack_uint(0)
            xdr.pack_uint(0)

        # Sequence counter.
        if 'counter' in kwargs:
            xdr.pack_uint(kwargs['counter'])
        else:
            xdr.pack_uint(0)

        # Frame check result.
        xdr.pack_uint(0)

        # Frame sync status.
        xdr.pack_uint(0)

        # Bit slip status.
        if 'bitslip' in kwargs:
            xdr.pack_uint(kwargs['bitslip'])
        else:
            xdr.pack_uint(0)

        # Telemetry delay.
        if 'delay' in kwargs:
            xdr.pack_uint(kwargs['delay'])
        else:
            xdr.pack_uint(0)

        # Frame length.
        xdr.pack_uint(len(data))

        # Sync word length.
        if 'synclength' in kwargs:
            xdr.pack_uint(kwargs['synclength'])
        else:
            xdr.pack_uint(0)

        # Decoder status.
        xdr.pack_uint(0)

        # Other status
        xdr.pack_uint(0)

        # Two unused words.
        xdr.pack_uint(0)
        xdr.pack_uint(0)

        # Extend with the TM data.
        output = xdr.get_buffer()

        # Extend with the raw data.
        output += data

        # Pad until 4 byte aligned.
        while(len(output) % 4) != 0:
            output += b'0'

        return super().pack(output, flow_id)

    def _process(self, raw: bytes):
        """
        Overloaded method fror message processing.
        """

        # Build the unpacker.
        xdr = xdrlib.Unpacker(raw[0:52])

        # Time tag.
        self.timetag1 = xdr.unpack_uint()
        self.timetag2 = xdr.unpack_uint()

        # Sequence counter.
        self.counter = xdr.unpack_uint()

        # Frame check result.
        self.framecheck = xdr.unpack_uint()

        # Frame sync status.
        self.framesync = xdr.unpack_uint()

        # Bit slip status.
        self.bitslip = xdr.unpack_uint()

        # Telemetry delay.
        self.delay = xdr.unpack_uint()

        # Frame length.
        self.length = xdr.unpack_uint()

        # Sync word length.
        self.synclength = xdr.unpack_uint()

        # Decoder status.
        self.status1 = xdr.unpack_uint()

        # Other status
        self.status2 = xdr.unpack_uint()

        # Two unused words.
        _ = xdr.unpack_uint()
        _ = xdr.unpack_uint()

        if self.length > 0:
            self.tmdata = raw[52:(52+self.length)]
        else:
            self.tmdata = b''
