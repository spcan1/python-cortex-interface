import logging
import socket
import sys


logger = logging.getLogger(__name__)


def sockerror_win32(e: socket.error, socketid=None, conntype=None) -> bool:
    """
    Logs socket errors for Windows.
    Returns True if it's a reason to close the connection.
    """

    if conntype is None:
        name = 'Connection Error'
        cid = ''
    else:
        name = conntype
        if socketid is None:
            cid = ''
        else:
            cid = f'{socketid}'

    # The Network is down.
    if e.winerror == 10050:
        logger.error(f'{name} {cid}: [WinError 10050] Network is down. Closing socket...') # noqa
        return True

    # The Network is unreachable.
    elif e.winerror == 10051:
        logger.error(f'{name} {cid}: [WinError 10051] Unreachable network. Closing socket...') # noqa
        return True

    # The Network dropped the connection.
    elif e.winerror == 10052:
        logger.error(f'{name} {cid}: [WinError 10052] Connection dropped by network. Closing socket...') # noqa
        return True

    # Software aborted the connection.
    elif e.winerror == 10053:
        logger.error(f'{name} {cid}: [WinError 10053] Connection dropped by software. Closing socket...') # noqa
        return True

    # Peer resetted the connection.
    elif e.winerror == 10054:
        logger.error(f'{name} {cid}: [WinError 10054] Connection dropped by peer. Closing socket...') # noqa
        return True

    # Socket is already shut down.
    elif e.winerror == 10058:
        logger.error(f'{name} {cid}: [WinError 10058] Connection is already shut down. Closing socket...') # noqa
        return True

    # Connection timed out.
    elif e.winerror == 10060:
        logger.error(f'{name} {cid}: [WinError 10060] Connection timed out. Closing socket...') # noqa
        return True

    # The host is down.
    elif e.winerror == 10064:
        logger.error(f'{name} {cid}: [WinError 10064] Host is down. Closing socket...') # noqa
        return True

    # Host is unreachable.
    elif e.winerror == 10065:
        logger.error(f'{name} {cid}: [WinError 10065] Host is unreachable. Closing socket...') # noqa
        return True

    # Socket is already shutting down.
    elif e.winerror == 10101:
        logger.error(f'{name} {cid}: [WinError 10101] Socket is shutting down. Closing socket...') # noqa
        return True

    else:
        return False


def sockerror_linux(e: socket.error, socketid=None, conntype=None) -> bool:
    """
    Logs socket errors for Linux.
    Returns True if it's a reason to close the connection.
    """

    if conntype is None:
        name = 'Connection Error'
        cid = ''
    else:
        name = conntype
        if socketid is None:
            cid = ''
        else:
            cid = f'{socketid}'

    # Peer has shut down one or both directions.
    if e.errno == 32:
        logger.error(f'{name} {cid}: [ErrNo 32] Connection dropped by peer. Closing socket...') # noqa

    # Network is down.
    elif e.errno == 50:
        logger.error(f'{name} {cid}: [ErrNo 50] Network is down. Closing socket...') # noqa

    # The Network is unreachable.
    elif e.errno == 51:
        logger.error(f'{name} {cid}: [ErrNo 51] Unreachable network. Closing socket...') # noqa
        return True

    # The Network dropped the connection.
    elif e.errno == 52:
        logger.error(f'{name} {cid}: [ErrNo 52] Connection dropped by network. Closing socket...') # noqa
        return True

    # Software aborted the connection.
    elif e.errno == 53:
        logger.error(f'{name} {cid}: [ErrNo 53] Connection dropped by software. Closing socket...') # noqa
        return True

    # Connection to host is not available.
    elif e.errno == 54:
        logger.error(f'{name} {cid}: [ErrNo 54] Connection to host unavailable. Closing socket...') # noqa
        return True

    # Socket is already connected.
    elif e.errno == 56:
        logger.error(f'{name} {cid}: [ErrNo 56] Socket is already connected. Closing socket...') # noqa
        return True

    # Socket is not connected.
    elif e.errno == 57:
        logger.error(f'{name} {cid}: [ErrNo 57] Socket is not connected. Closing socket...') # noqa
        return True

    # Socket is already shut down.
    elif e.errno == 58:
        logger.error(f'{name} {cid}: [ErrNo 58] Connection is already shut down. Closing socket...') # noqa
        return True

    # Connection timed out.
    elif e.errno == 60:
        logger.error(f'{name} {cid}: [ErrNo 60] Connection timed out. Closing socket...') # noqa
        return True

    # Connection was refused.
    elif e.errno == 61:
        logger.error(f'{name} {cid}: [ErrNo 61] Connection was refused. Closing socket...') # noqa
        return True

    # The host is down.
    elif e.errno == 64:
        logger.error(f'{name} {cid}: [ErrNo 64] Host is down. Closing socket...') # noqa
        return True

    # Host is unreachable.
    elif e.errno == 65:
        logger.error(f'{name} {cid}: [ErrNo 65] Host is unreachable. Closing socket...') # noqa
        return True

    # Requestd machine not on the network.
    elif e.errno == 80:
        logger.error(f'{name} {cid}: [ErrNo 80] Peer machine not on the network. Closing socket...') # noqa
        return True

    # Link has been severed.
    elif e.errno == 82:
        logger.error(f'{name} {cid}: [ErrNo 82] Link to peer has been severed. Closing socket...') # noqa
        return True

    # Protocol error.
    elif e.errno == 86:
        logger.error(f'{name} {cid}: [ErrNo 86] Protocol error. Closing socket...') # noqa
        return True

    # Destination address changed.
    elif e.errno == 89:
        logger.error(f'{name} {cid}: [ErrNo 89] Destination address changed. Closing socket...') # noqa
        return True

    # Connectino aborted by peer.
    elif e.errno == 90:
        logger.error(f'{name} {cid}: [ErrNo 90] Connection aborted by peer. Closing socket...') # noqa
        return True

    return False


if sys.platform.startswith("win32"):
    sockerror = sockerror_win32
elif sys.platform.startswith("linux"):
    sockerror = sockerror_linux
