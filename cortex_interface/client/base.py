import socket
import logging

from cortex_interface.common.error import sockerror
from cortex_interface.common.messages import CortexMessage

logger = logging.getLogger(__name__)


class CortexClient:

    """Base class for all Cortex clients."""

    _port = None

    def __init__(self, host='localhost', port=None):

        # Initial configuration.
        self.host = host
        self.port = port

    def connect(self):
        # Build the TCP IP connection.
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect to the host and port.
        try:
            self.socket.connect((self.host, self.port))
        except socket.error as e:
            sockerror(e)
            raise e

    def disconnect(self):
        self.socket.shutdown(socket.SHUT_RDWR)

    def _send(self, data):
        logger.debug(f'Sending {len(data)} bytes to {self.host}:{self.port}')
        try:
            self.socket.send(data)
        except socket.error as e:
            sockerror(e)

    def _recv(self, cls: CortexMessage) -> CortexMessage:
        try:
            return cls.recv(self.socket)
        except socket.error as e:
            sockerror(e)
            return None
