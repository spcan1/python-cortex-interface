from .base import CortexClient

from cortex_interface.common.messages import TelemetryPacket, TelemetryRequest


class TelemetryClient(CortexClient):
    """Controls the flow of telemetry data."""

    port = 3070

    def __init__(self, host='localhost'):
        super().__init__(host, self.port)

    def permanent(self, tries=0, maxtries=10):
        """Starts a continuous stream of telemetry frames with the given IP"""
        self._send(TelemetryRequest.permanent(0))

    def singleframe(self, tries=0, maxtries=10):
        """Starts a continuous stream of telemetry frames with the given IP"""
        self._send(TelemetryRequest.singleframe(0))

    def dummy(self, tries=0, maxtries=10):
        """Starts a continuous stream of telemetry frames with the given IP"""
        self._send(TelemetryRequest.dummy(0))

    def receive_frame(self):
        """
        Attempts to receive a TM frame.
        'Manual' way of frame reception.
        """

        return self._recv(TelemetryPacket)

    def frame_indication(self, frame):
        """
        Overloadable method for automatic frame reception.
        """

        pass

    def stop(self):
        """Sends a stop command and closes the connection."""

        self._send(TelemetryRequest.stop(0))

        self.disconnect()
