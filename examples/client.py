import logging
import time

from cortex_interface.client import TelemetryClient


#host = 'localhost'
#host = '127.0.0.1'
host = '0.0.0.0'


def run():

    client = TelemetryClient(host)

    client.connect()

    client.dummy(0)

    # This is diagnostic data.
    # ########################################################################
    start = time.time()
    last = time.time()
    data = 0
    # ########################################################################

    while time.time() - start < 6:
        frame = client.receive_frame()

        if frame is None:
            print("No frames received")
        else:
            pass

        # This is diagnostic data.
        # ########################################################################
        data += float(len(frame.tmdata))
        delta = time.time() - last

        if delta > 1.0:
            print(f'Throughput : {(data / delta) / 1024} kiB/s')
            print(f'Timetag: {frame.timetag1} | {frame.timetag2}')
            data = 0
            last = time.time()
        # ########################################################################

        time.sleep(0.01)

    client.stop()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    run()
