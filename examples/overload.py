import logging
import time

from cortex_interface.client import TelemetryClient


#host = 'localhost'
#host = '127.0.0.1'
host = '0.0.0.0'


def run():
    clients = [TelemetryClient(host) for _ in range(0, 24)]

    for client in clients:
        client.connect()
        client.dummy(0)

    start = time.time()
    last = time.time()
    mtdata = 0
    stdata = [0 for _ in range(0, 24)]

    while time.time() - start < 6.0:

        for (i, client) in enumerate(clients):
            frame = client.receive_frame()

            if frame is not None:
                mtdata += float(len(frame.tmdata))
                stdata[i] += float(len(frame.tmdata))

        delta = time.time() - last

        if delta > 1.0:
            print(f'Full throughput: {(mtdata * 8.0) / 1024.0} kbps')

            meandata = sum(stdata) / len(stdata)

            print(f'Average throughput: {(meandata * 8.0) / 1024.0} kbps')

            mtdata = 0
            stdata = [0 for _ in range(0, 24)]
            last = time.time()

        time.sleep(0.01)

    for client in clients:
        client.stop()
        time.sleep(0.1)


def mixed():

    mix = [0, 4, 6, 12, 15, 20, 9, 18]

    clients = [TelemetryClient(host) for _ in range(0, 24)]

    for client in clients:
        client.connect()
        client.dummy(0)

    start = time.time()
    last = time.time()
    mtdata = 0
    stdata = [0 for _ in range(0, 24)]

    while time.time() - start < 6.0:

        for (i, client) in enumerate(clients):
            frame = client.receive_frame()

            if frame is not None:
                mtdata += float(len(frame.tmdata))
                stdata[i] += float(len(frame.tmdata))

        delta = time.time() - last

        if delta > 1.0:
            print(f'Full throughput: {(mtdata * 8.0) / 1024.0} kbps')

            meandata = sum(stdata) / len(stdata)

            print(f'Average throughput: {(meandata * 8.0) / 1024.0} kbps')

            mtdata = 0
            stdata = [0 for _ in range(0, 24)]
            last = time.time()

        time.sleep(0.01)

    print('Partial stoppage')

    for i in mix:
        clients[i].stop()
        time.sleep(0.1)

    print('Reconnecting...')

    for i in mix:
        clients[i].connect()
        clients[i].dummy(0)

    print('Second round')

    start = time.time()

    while time.time() - start < 6.0:

        for (i, client) in enumerate(clients):
            frame = client.receive_frame()

            if frame is not None:
                mtdata += float(len(frame.tmdata))
                stdata[i] += float(len(frame.tmdata))

        delta = time.time() - last

        if delta > 1.0:
            print(f'Full throughput: {(mtdata * 8.0) / 1024.0} kbps')

            meandata = sum(stdata) / len(stdata)

            print(f'Average throughput: {(meandata * 8.0) / 1024.0} kbps')

            mtdata = 0
            stdata = [0 for _ in range(0, 24)]
            last = time.time()

        time.sleep(0.01)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
#    run()
#    time.sleep(0.01)
    mixed()
