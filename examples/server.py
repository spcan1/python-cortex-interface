import logging

from cortex_interface.server import CortexUser


#host = 'localhost'
#host = '127.0.0.1'
host = '0.0.0.0'


def run():

    server = CortexUser(host)

    server.process_channel_file('default.yml')

    server.startup()

    input('Press [Enter] to stop the server:\n')

    server.shutdown()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    run()
